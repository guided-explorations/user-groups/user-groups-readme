

Table Of Contents

[[_TOC_]]

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for a new capability of GitLab.

* **Publish Date**: 2019-12-27
* **GitLab Version Released On**: 12.6
* **GitLab Edition Required**: 
  * For overall solution: [![FC](./images/FC.png)](https://about.gitlab.com/features/) 
  * For leveraging custom user groups in many places within GitLab (see table at the bottom of this document): ![FC](./images/PS.png)

  * [Click to see Features by Edition](https://about.gitlab.com/features/) 
* **References & Featured In**: 
  * Video: [Rich Change Controls for Building Workflows You Can Trust](https://youtu.be/uW95PV8d-w8)

## Demonstrated Design Requirements and Desirements

### Dedicated User Group Hierarchies

This documentation is speaking about the GitLab group heirarchies you can find within this group: https://gitlab.com/guided-explorations/user-groups

GitLab groups can be configured to function as dedicated group hierarchies, similar to an operating system.

What is demonstrated:

- **GitLab Groups Setting** "[Allowed to create projects](https://docs.gitlab.com/ee/user/group/#default-project-creation-level) = No one"![FC](./images/FC.png) to ensure repositories are not created in a user group.
- **GitLab Groups Setting** "Allowed to create subgroups = Owners" ![FC](./images/FC.png) to prevent subgroups being created by anyone in the groups.
- **GitLab Member Role** "Maintainer" ![FC](./images/FC.png) to enable a group member to manage members (delegation of group membership)
- **Best Practice** "Never Add Users to Non-Leaf Groups" ![FC](./images/FC.png) only add users to bottom of the hierarchy groups that have no sub-groups (leaf groups).

### Downward Membership Inheritance Work Arounds

GitLab user groups have their heritage in Git collaborative systems which inherit membership down the hierarchy.  These examples demonstrate the following work arounds to prevent inadvertant group membership assignment by adding users in a parent group.

- **Workaround Configuration** ![FC](./images/FC.png) - ONLY add users to leaf groups that do not have any sub-groups

- **Workaround Configuration** ![FC](./images/FC.png) - Make the non-leaf groups description start with "NO USERS IN THIS GROUP" - this helps communicate both:

  - To not place users in the group.
  - That there are no users in the group when browsing for groups to select.

- **Workaround Configuration** ![FC](./images/FC.png) - Add group icons that clearly communicate that the groups are User Groups.  For example:

  ![group-team](images/group-team.png)

- **Workaround Configuration** ![FC](./images/FC.png) - Use a special icon to clearly communicate not to add users, nor to expect users to be present in, non-leaf groups. For example:

  ![group-team](images/group-team-nousers.png)

### GitLab.com SaaS Limitations

When using GitLab.com, you do not have administrative access to the instance as you do with self-hosted.  This creates some limitations:

* All SSO solutions for GitLab.com require that your users are created in your "namespace" (or root group) as guests.  This means you cannot create a Projects only group structure where users only have explicit permissions.  All users will have guest to everything at a minimum.

### GitLab Group Assignment Pass Through Permissions Limitations

When GitLab **User Groups** are assigned to projects, users cannot have effective permissions that are higher than their membership in the source group.  So if Sally has "Guest" in "DevOps Engineers" and "DevOps Engineers" is made a member of the project "Application Service A" as a "Developer", Sally will only have the effective permissions of "Guest".  This situation is hard to debug because there is nothing in the interface that shows Sally's effective permissions are being limited.

You can downgrade group permissions.  So if Amir is a "Maintainer" in "DevOps Engineers" and you assign "DevOps Engineers" to the project "Payment Microservice" as "Developer", Amir's effective permissions will be "Developer".  A challenge created by this approach is that by being a maintainer in "DevOps Engineers", Amir can add other users to the group and create subgroups.  This situation is a little easier to understand in the user interface of the project. [Maximum Permissions Documentation](https://docs.gitlab.com/ee/user/project/members/share_project_with_groups.html#maximum-access-level)

### Adding Groups To Project Membership Before Adding to Branch Protections or Approval Rules

Branch protections and Merge Approval Rules can only be configured on a group if the group has first been added as a member of the project.  Since this restriction does not exist when adding members to a project or group, it can be challenging to remember the pre-step.  Additionally, the "Group" section heading in Branch protections does not display unless there is at least one group added as a project member - so it can be hard to discover this is possible when exploring the product capabilities through the user interface.

### Group Membership In Groups Is New

The pass through behavior is the same as group memberships in projects.  The user can have the role defined in the source group downgraded to a lower level, but not upgraded to a higher level, when assigning the group to a given role in another group.

### Features That Can Be Configured To Use Custom User Groups

| Feature                                                      | Control Scope    | Edition                |
| ------------------------------------------------------------ | ---------------- | ---------------------- |
| [Total Number of Approvals Required](https://gitlab.com/help/user/project/merge_requests/merge_request_approvals#merge-request-approvals-starter) | Repo             | ![FC](./images/SB.png) |
| [CODEOWNERS](https://docs.gitlab.com/ee/user/project/code_owners.html) as [Eligible MR Approvers](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html#code-owners-as-eligible-approvers-starter) | File / Directory | ![FC](./images/SB.png) |
| [Branch Protections](https://docs.gitlab.com/ee/user/project/protected_branches.html#restricting-push-and-merge-access-to-certain-users-starter) | Branch           | ![FC](./images/SB.png) |
| [Multiple Required Approval Rules](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html#multiple-approval-rules-premium) (Multiple Groups) (Group must be added to repo as share group before it is available to add in approval rules.) | Repo             | ![FC](./images/PS.png) |
| [CODEOWNERS](https://docs.gitlab.com/ee/user/project/code_owners.html) as [Required Approvers in Branch Protections](https://docs.gitlab.com/ee/user/project/protected_branches.html#protected-branches-approval-by-code-owners-premium) | Branch           | ![FC](./images/PS.png) |

